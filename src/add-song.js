import React from "react";
import Autosuggest from "react-autosuggest";
import "./add-song.css";
import { connect } from "react-redux";
import {
  fetchAutosuggestionSongs,
  autosuggestionSearchString,
  clearAutoSuggestionSongs,
  clearAutoSuggestionSearchBox,
  addSong
} from "./actions";

// todo: convert to function?
export class AddSong extends React.Component {
  onChange = (event, { newValue }) => {
    this.props.dispatch(autosuggestionSearchString(newValue));
  };

  getSuggestionValue = suggestion =>
    `${suggestion.songName} by ${suggestion.artist}`;

  renderSuggestion = suggestion => (
    <div>
      <strong>{suggestion.songName}</strong>
      <div>{suggestion.artist}</div>
    </div>
  );

  onSuggestionsFetchRequested = () => {
    this.props.dispatch(
      fetchAutosuggestionSongs(this.props.autosuggestionSearchString)
    );
  };

  onSuggestionsClearRequested = () => {
    this.props.dispatch(clearAutoSuggestionSongs());
  };

  onSuggestionSelected = (event, { suggestion }) => {
    this.props.dispatch(addSong(suggestion.songName, suggestion.artist));
    this.props.dispatch(clearAutoSuggestionSearchBox());
  };

  render() {
    const inputProps = {
      placeholder: "Add Song",
      value: this.props.autosuggestionSearchString,
      onChange: this.onChange
    };

    return (
      <div className="addSongContainer">
        <form className="addSongForm">
          <Autosuggest
            suggestions={this.props.autosuggestionSearchResults}
            getSuggestionValue={this.getSuggestionValue}
            renderSuggestion={this.renderSuggestion}
            inputProps={inputProps}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
            onSuggestionSelected={this.onSuggestionSelected}
          />
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  autosuggestionSearchResults: state.app.autosuggestionSearchResults,
  autosuggestionSearchString: state.app.autosuggestionSearchString
});

export default connect(mapStateToProps)(AddSong);
