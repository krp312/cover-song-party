import {
  FETCH_SONGS_REQUEST,
  FETCH_SONGS_SUCCESS,
  FETCH_SONGS_ERROR,
  AUTOSUGGESTION_SEARCH_STRING,
  CLEAR_AUTOSUGGESTION_SONGS,
  CLEAR_AUTOSUGGESTION_SEARCH_BOX,
  ADD_SONG,
  SAVE_SONG_INSTRUMENTS
} from '../actions';
import { seedState } from '../seed-data.js';
import { initialInstruments } from '../utility.js';

// eslint-disable-next-line
const initialState = {
  autosuggestionSearchString: '',
  autosuggestionSearchResults: [],
  matchedSongs: [],
  loading: false,
  error: null,
  savedSongs: []
};

// todo: refactor this and try switch-case syntax
// reference: https://redux.js.org/recipes/structuring-reducers/refactoring-reducer-example/
// todo: maybe make an autosuggestion reducer
const reducer = (state = seedState, action) => {
  if (action.type === FETCH_SONGS_REQUEST) {
    return Object.assign({}, state, {
      loading: true,
      error: null
    });
  } else if (action.type === FETCH_SONGS_SUCCESS) {
    return Object.assign({}, state, {
      loading: false,
      error: null,
      autosuggestionSearchResults: action.songs
    });
  } else if (action.type === FETCH_SONGS_ERROR) {
    return Object.assign({}, state, {
      loading: false,
      error: action.error
    });
  } else if (action.type === AUTOSUGGESTION_SEARCH_STRING) {
    return Object.assign({}, state, {
      autosuggestionSearchString: action.string
    });
  } else if (action.type === CLEAR_AUTOSUGGESTION_SONGS) {
    return Object.assign({}, state, {
      autosuggestionSearchResults: []
    });
  } else if (action.type === CLEAR_AUTOSUGGESTION_SEARCH_BOX) {
    return Object.assign({}, state, {
      autosuggestionSearchString: ''
    });
  } else if (action.type === ADD_SONG) {
    const song = state.savedSongs.find(song => {
      return song.songName === action.songName && song.artist === action.artist;
    });

    // todo: does initialInstruments() have to be a function call?
    const newSong = {
      songName: action.songName,
      artist: action.artist,
      instruments: initialInstruments()
    };

    if (!song) {
      return Object.assign({}, state, {
        savedSongs: [...state.savedSongs, newSong]
      });
    }
  } else if (action.type === SAVE_SONG_INSTRUMENTS) {
    // todo: better way?
    const savedSongs = state.savedSongs.map((song, index) => {
      if (index === action.index) {
        song.instruments = action.newInstruments;
        return song;
      } else {
        return song;
      }
    });

    return Object.assign({}, state, { savedSongs });
  }
  return state;
};

export default reducer;
