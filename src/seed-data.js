export const seedState = {
  autosuggestionSearchString: '',
  autosuggestionSearchResults: [],
  matchedSongs: [],
  loading: false,
  error: null,
  savedSongs: [
    {
      songName: 'Fluorescent Adolescent',
      artist: 'Arctic Monkeys',
      instruments: {
        bass: true,
        drums: true,
        guitar: false,
        vocals: false
      }
    },
    {
      songName: 'Come as You Are',
      artist: 'Nirvana',
      instruments: {
        bass: false,
        drums: false,
        guitar: false,
        vocals: true
      }
    },
    {
      songName: 'The Hunger',
      artist: 'Bat for Lashes',
      instruments: {
        bass: true,
        drums: false,
        guitar: true,
        vocals: true
      }
    }
  ]
};

export const matches = [
  {
    user: 'melissa',
    songName: 'Fluorescent Adolescent',
    artist: 'Arctic Monkeys',
    instruments: {
      bass: false,
      drums: false,
      guitar: true,
      vocals: false
    }
  },
  {
    user: 'natasha',
    songName: 'Come as You Are',
    artist: 'Nirvana',
    instruments: {
      bass: true,
      drums: true,
      guitar: false,
      vocals: false
    }
  },
  {
    user: 'natasha',
    songName: 'The Hunger',
    artist: 'Bat for Lashes',
    instruments: {
      bass: false,
      drums: true,
      guitar: false,
      vocals: false
    }
  }
];
