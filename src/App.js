import React from 'react';
import './App.css';
import AddSong from './add-song';
import Songs from './songs';
import Nav from './nav';
import Matches from './matches';
import { BrowserRouter as Router, Route } from 'react-router-dom';

const App = props => {
  return (
    <Router>
      <div className="appContainer">
        <Nav />
        <Route
          exact
          path={['/', '/songs']}
          render={() => (
            <div>
              <AddSong />
              <Songs />
            </div>
          )}
        />
        <Route exact path="/matches" component={Matches} />
      </div>
    </Router>
  );
};

export default App;
