import React from "react";
import { connect } from "react-redux";
import "./instrument-selector.css";
import { reduxForm, FieldArray } from "redux-form";
import Song from "./song";

const renderInstruments = ({ fields }) => {
  return (
    <div>
      {fields.map((song, index) => (
        <Song key={song} song={song} index={index} />
      ))}
    </div>
  );
};

let Instruments = () => {
  return <FieldArray name="songs" component={renderInstruments} />;
};

Instruments = reduxForm({
  form: "songs",
  enableReinitialize: true
})(Instruments);

Instruments = connect(state => ({
  initialValues: {
    songs: state.app.savedSongs
  }
}))(Instruments);

export default Instruments;
