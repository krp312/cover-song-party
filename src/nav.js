import React from 'react';
import { Link } from 'react-router-dom';

const Nav = () => {
  return (
    <div>
      <Link to="/songs">Songs</Link>
      <Link to="/matches">Matches</Link>
    </div>
  );
};

export default Nav;
