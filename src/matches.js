import React from 'react';
import { matches } from './seed-data.js';

const Matches = () => {
  return matches.map(match => {
    return (
      <div>
        <p>{match.user}</p>
        <p>{match.songName}</p>
      </div>
    );
  });
};

export default Matches;
