import { INSTRUMENTS } from "./constants";

export const initialInstruments = () => {
  const instruments = {};
  INSTRUMENTS.forEach(instrument => {
    instruments[instrument] = false;
  });
  return instruments;
};
