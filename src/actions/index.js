export const FETCH_SONGS_REQUEST = 'FETCH_SONGS_REQUEST';
export const fetchSongsRequest = () => ({
  type: FETCH_SONGS_REQUEST
});

export const FETCH_SONGS_SUCCESS = 'FETCH_SONGS_SUCCESS';
export const fetchSongsSuccess = songs => ({
  type: FETCH_SONGS_SUCCESS,
  songs
});

export const FETCH_SONGS_ERROR = 'FETCH_SONGS_ERROR';
export const fetchSongsError = error => ({
  type: FETCH_SONGS_ERROR,
  error
});

export const AUTOSUGGESTION_SEARCH_STRING = 'AUTOSUGGESTION_SEARCH_STRING';
export const autosuggestionSearchString = string => ({
  type: AUTOSUGGESTION_SEARCH_STRING,
  string
});

export const CLEAR_AUTOSUGGESTION_SONGS = 'CLEAR_AUTOSUGGESTION_SONGS';
export const clearAutoSuggestionSongs = () => ({
  type: CLEAR_AUTOSUGGESTION_SONGS
});

export const CLEAR_AUTOSUGGESTION_SEARCH_BOX =
  'CLEAR_AUTOSUGGESTION_SEARCH_BOX';
export const clearAutoSuggestionSearchBox = () => ({
  type: CLEAR_AUTOSUGGESTION_SEARCH_BOX
});

export const ADD_SONG = 'ADD_SONG';
export const addSong = (artist, songName) => ({
  type: ADD_SONG,
  artist,
  songName
});

export const SAVE_SONG_INSTRUMENTS = 'SAVE_SONG_INSTRUMENTS';
export const saveSongInstruments = (index, newInstruments) => ({
  type: SAVE_SONG_INSTRUMENTS,
  index,
  newInstruments
});

export const fetchAutosuggestionSongs = song => dispatch => {
  // todo: display a loading thingy
  dispatch(fetchSongsRequest());
  fetch(
    `http://ws.audioscrobbler.com/2.0/?method=track.search&api_key=8d0f1c23cd764bd15c99016916a946d0&format=json&track=${song}`
  )
    .then(res => {
      // todo: why?
      return res.json();
    })
    .then(res => {
      if (res.results !== undefined) {
        const searchResults = res.results.trackmatches.track.map(song => {
          return { songName: song.name, artist: song.artist };
        });
        dispatch(fetchSongsSuccess(searchResults));
      }
    })
    .catch(error => {
      // todo: display this
      dispatch(fetchSongsError(error));
    });
};
