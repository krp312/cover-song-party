import React from "react";
import "./instrument-selector.css";
import { Field } from "redux-form";
import InstrumentSelector from "./instrument-selector";
import { connect } from "react-redux";
// todo: import one method only?
import _ from "lodash";
import { saveSongInstruments } from "./actions";

const renderSongMetadata = props => {
  return <div>{props.input.value}</div>;
};

const Song = ({ song, index, savedSongs, changedSongs, dispatch }) => {
  const originalInstruments = savedSongs[index].instruments;
  const changedInstruments = changedSongs[index].instruments;

  let saveButton;
  if (!_.isEqual(originalInstruments, changedInstruments)) {
    saveButton = (
      <button
        onClick={() =>
          dispatch(saveSongInstruments(index, changedSongs[index].instruments))
        }
      >
        Save
      </button>
    );
  }

  return (
    <div>
      <Field name={`${song}.songName`} component={renderSongMetadata} />
      <Field name={`${song}.artist`} component={renderSongMetadata} />
      <InstrumentSelector song={song} />
      {saveButton}
    </div>
  );
};

const mapStateToProps = state => ({
  savedSongs: state.app.savedSongs,
  // todo: below. wtf?
  changedSongs: state.form.songs.values.songs
});

export default connect(mapStateToProps)(Song);
