import React from "react";
import "./instrument-selector.css";
import { Field } from "redux-form";
import { INSTRUMENTS } from "./constants.js";

const InstrumentSelector = ({ fields, song, handleClick }) => {
  return INSTRUMENTS.map(instrument => (
    <div key={instrument}>
      <label>{instrument.charAt(0).toUpperCase() + instrument.slice(1)}</label>
      <Field
        name={`${song}.instruments.${instrument}`}
        type="checkbox"
        component="input"
      />
    </div>
  ));
};

export default InstrumentSelector;
