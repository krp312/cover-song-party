import React from "react";
import { Field, FieldArray, reduxForm } from "redux-form";

const renderSongMetadata = props => {
  return <div>{props.input.value}</div>;
};

const renderInstruments = ({ fields }) => {
  return (
    <div>
      {fields.map(song => {
        const instrumentCheckboxes = ["bass", "drums", "guitar", "vocals"].map(
          instrument => {
            return (
              <div>
                <label>
                  {instrument.charAt(0).toUpperCase() + instrument.slice(1)}
                </label>
                <Field
                  name={`${song}.instruments.${instrument}`}
                  type="checkbox"
                  component="input"
                />
              </div>
            );
          }
        );

        return (
          <div>
            <Field name={`${song}.songName`} component={renderSongMetadata} />
            <Field name={`${song}.artist`} component={renderSongMetadata} />
            {instrumentCheckboxes}
          </div>
        );
      })}
    </div>
  );
};

const Instruments = () => {
  return <FieldArray name="songs" component={renderInstruments} />;
};

export default reduxForm({
  form: "fieldArrays",
  initialValues: {
    songs: [
      {
        songName: "The Hunger",
        artist: "Bat for Lashes",
        instruments: {
          bass: false,
          drums: false,
          guitar: false,
          vocals: true
        }
      },
      {
        songName: "Waaaa",
        artist: "Mr. Owen",
        instruments: {
          bass: false,
          drums: false,
          guitar: true,
          vocals: true
        }
      }
    ]
  }
})(Instruments);
